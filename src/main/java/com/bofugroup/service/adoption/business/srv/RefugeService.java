package com.bofugroup.service.adoption.business.srv;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.bofugroup.service.adoption.business.dto.Refuge;
import com.bofugroup.service.adoption.exception.ExceptionRefugeNotFound;
import com.bofugroup.service.adoption.exception.ResourceNotFoundException;
import com.bofugroup.service.adoption.repository.RefugeRepository;

@Service
public class RefugeService 
{

	@Autowired 
	RefugeRepository refugeRepo;
	
	public List<Refuge>all()
	{
		return (List<Refuge>) refugeRepo.findAll();
	}
	
	public Optional<Refuge> getId(Long id)
	{
		return refugeRepo.findById(id);
	}
	
	public Refuge save(Refuge refuge) 
	{
		Refuge response = null;
		if(refuge != null)
			response = refugeRepo.save(refuge);
		else 
			throw new ExceptionRefugeNotFound();
		
		return response;
	}
	
	public ResponseEntity<?> delete(Long idRefuge) 
	{
		  return refugeRepo.findById(idRefuge).map(refuge -> {
	            refugeRepo.delete(refuge);
	            return ResponseEntity.ok().build();
	        }).orElseThrow(() -> new ResourceNotFoundException("RefugeId " + idRefuge+ " not found"));
	}
	
	public Refuge update(Long id, @RequestBody Refuge refugeRequest) 
	{
		
		return refugeRepo.findById(id).map(refuge -> {
            refuge.setDescription(refugeRequest.getDescription());
            refuge.setDirection(refugeRequest.getDirection());
            refuge.setFundation_date(refugeRequest.getFundation_date());
            refuge.setName(refugeRequest.getName());
            refuge.setId_Ciudad(refugeRequest.getId_Ciudad());
            refuge.setId_User(refugeRequest.getId_User());
            return refugeRepo.save(refuge);
        }).orElseThrow(() -> new ResourceNotFoundException("RefugeId " + id + " not found"));
	}
}
