package com.bofugroup.service.adoption.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bofugroup.service.adoption.business.dto.Vaccine;


@Repository
public interface VaccineRepository extends CrudRepository<Vaccine, Long>
{
	//List<Vaccine> findbyRAId(Long IdRefuge,Long IdAnimal); 
}
