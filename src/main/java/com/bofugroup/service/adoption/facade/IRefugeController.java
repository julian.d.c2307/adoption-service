package com.bofugroup.service.adoption.facade;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.bofugroup.service.adoption.facade.dto.DTOinRefuge;

public interface IRefugeController 
{

	public ResponseEntity<?> agregarRefugio(DTOinRefuge dtoinrefuge);
	public HttpStatus deleteRefuge(Long idRefuge);
	public HttpStatus updateRefuge(Long id,DTOinRefuge dtoinRefuge);
}
