package com.bofugroup.service.adoption.business.dto;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "Refuge_tbl")
public class Refuge {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long idRefuge;
	@Column(name = "name",nullable=false)
	String name;
	@Column(name = "direction",nullable=false)
	String direction;
	@Column(name = "fundation_date",nullable=false)
	Timestamp fundation_date;
	@Column(name = "description",nullable=false)
	String description;
	@Column(name = "id_ciudad",nullable=false)
	Long id_Ciudad;
	@Column(name = "id_user",nullable= true)
	Long id_User;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_refuge")
	List<Photo_Refuge> listPhotos;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_refuge")
	List<Animal> listAnimals;
	
	//@Column(name = "user",nullable=true)
	//User user;

	public Refuge() {
		super();
	}

	
	public Refuge(String name, String direction, Timestamp fundation_date, String description, Long id_Ciudad,
			Long id_User) {
		super();
		this.name = name;
		this.direction = direction;
		this.fundation_date = fundation_date;
		this.description = description;
		this.id_Ciudad = id_Ciudad;
		this.id_User = id_User;
	}


	public Long getRefugeId() {
		return idRefuge;
	}
	public void setRefugeId(Long refugeId) {
		idRefuge = refugeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public Timestamp getFundation_date() {
		return fundation_date;
	}
	public void setFundation_date(Timestamp fundation_date) {
		this.fundation_date = fundation_date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId_Ciudad() {
		return id_Ciudad;
	}
	public void setId_Ciudad(Long id_Ciudad) {
		this.id_Ciudad = id_Ciudad;
	}
	public Long getId_User() {
		return id_User;
	}
	public void setId_User(Long id_User) {
		this.id_User = id_User;
	}
}
