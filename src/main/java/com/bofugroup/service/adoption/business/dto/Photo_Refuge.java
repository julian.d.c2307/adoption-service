package com.bofugroup.service.adoption.business.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "photo_refuge")
public class Photo_Refuge 
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	//byte image[];
	@Column(name = "urlphoto", nullable = true)
	String urlphoto;
	
	@ManyToOne
	@JoinColumn(name = "id_refuge")
	Refuge refuge;
	
	public Photo_Refuge(){}

	public Photo_Refuge( String urlphoto, Refuge refuge) {
		super();
		this.urlphoto = urlphoto;
		this.refuge = refuge;
	}

	public String getUrlphoto() {
		return urlphoto;
	}


	public void setUrlphoto(String urlphoto) {
		this.urlphoto = urlphoto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
*/
	public Refuge getRefuge() {
		return refuge;
	}

	public void setRefuge(Refuge refuge) {
		this.refuge = refuge;
	}
	
	
}
