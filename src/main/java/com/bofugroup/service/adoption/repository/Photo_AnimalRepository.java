package com.bofugroup.service.adoption.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bofugroup.service.adoption.business.dto.Photo_Animal;

@Repository
public interface Photo_AnimalRepository extends CrudRepository<Photo_Animal, Long> 
{
	
}
