package com.bofugroup.service.adoption.facade.mapper;

import java.util.List;

import com.bofugroup.service.adoption.business.dto.Refuge;
import com.bofugroup.service.adoption.facade.dto.DTOinRefuge;
import com.bofugroup.service.adoption.facade.dto.DTOoutRefuge;


public interface IRefugeMapper 
{
	DTOoutRefuge mapOutRefuge(Refuge refuge);
	Refuge mapInRefuge(DTOinRefuge dtoInRefuge);
	List<DTOoutRefuge> mapOutListRefuge(List<Refuge> refuge);
}
