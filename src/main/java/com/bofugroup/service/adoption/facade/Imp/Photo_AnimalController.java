package com.bofugroup.service.adoption.facade.Imp;

import java.net.URI;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bofugroup.service.adoption.business.dto.Photo_Animal;
import com.bofugroup.service.adoption.business.srv.Photo_AnimalService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public class Photo_AnimalController {
	
	@Autowired
	Photo_AnimalService photoservice;
	
	private static final Logger LOGGER =LoggerFactory.getLogger(Photo_AnimalController.class);

	@ApiOperation(value = "save a Photo-Animal in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully save in bd"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping("/api/refuges/{RefugeId}/Animal/{AnimalId}/Photo")
	public Photo_Animal addPhotoAnimal(@PathVariable(value = "RefugeId")Long RefugeId,
			@PathVariable(value = "AnimalId")Long AnimalId,
			@RequestBody Photo_Animal photoanimal) {
		LOGGER.info("guardar foto a animal en refugio en bd");
		return photoservice.savePhotoAnimal(RefugeId, AnimalId, photoanimal);
	}
	@ApiOperation(value = "update a Photo-Animal in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PutMapping("api/refuges/{RefugeId}/Animals/{AnimalId}/Photos/{PhotoId}")
	public Photo_Animal updatePhotoAnimal(@PathVariable(value = " RefugeId")Long RefugeId,
			@PathVariable(value = "AnimalId")Long AnimalId,
			@PathVariable(value = "PhotoId")Long PhotoId,
			@Valid @RequestBody Photo_Animal photoanimalRequest) {
		LOGGER.info("actualizar foto en bd");
		return photoservice.updatePhotoAnimal(RefugeId, AnimalId, PhotoId, photoanimalRequest);
	}
	
	@ApiOperation(value = "Delete a Photo-Animal in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@DeleteMapping("/api/refuge/{RefugeId}/Animals/{AnimalId}/Photos/{PhotoId}")
	public ResponseEntity<?> deletePhotoAnimal(@PathVariable(value = "RefugeId")Long RefugeId,
			@PathVariable(value = "AnimalId")Long AnimalId,
			@PathVariable(value = "PhotoId")Long PhotoId){
		LOGGER.info("eliminar foto de animal en bd");
		return photoservice.deletePhotoAnimal(RefugeId, AnimalId, PhotoId);
	}
	
}
