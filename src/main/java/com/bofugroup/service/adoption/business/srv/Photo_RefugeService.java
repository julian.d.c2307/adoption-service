package com.bofugroup.service.adoption.business.srv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.bofugroup.service.adoption.business.dto.Photo_Refuge;
import com.bofugroup.service.adoption.exception.ResourceNotFoundException;
import com.bofugroup.service.adoption.repository.Photo_RefugeRepository;
import com.bofugroup.service.adoption.repository.RefugeRepository;

@Service
public class Photo_RefugeService {

	@Autowired
	Photo_RefugeRepository photorepo;

	@Autowired
	RefugeRepository refugerepo;

	/*
	 * public List<Photo_Refuge>all(Long RefugeId,Pageable pageable) { return
	 * (List<Photo_Refuge>) photorepo.findRefugeId(RefugeId, pageable); }
	 */

	public Photo_Refuge savePhoto(Long RefugeId, @RequestBody Photo_Refuge photo) {
		return refugerepo.findById(RefugeId).map(refuge -> {
			photo.setRefuge(refuge);
			return photorepo.save(photo);
		}).orElseThrow(() -> new ResourceNotFoundException("Refuge " + RefugeId + " not found"));
	}

	public Photo_Refuge update(Long RefugeId, Long PhotoId, @RequestBody Photo_Refuge photoRequest) {
		if (!refugerepo.existsById(RefugeId)) {
			throw new ResourceNotFoundException("RefugetId " + RefugeId + " not found");
		}

		return photorepo.findById(PhotoId).map(photo -> {
			photo.setUrlphoto(photoRequest.getUrlphoto());
			return photorepo.save(photo);
		}).orElseThrow(() -> new ResourceNotFoundException("PhotoId " + PhotoId + "not found"));
	}

	public ResponseEntity<?> delete(Long RefugeId, Long PhotoId) {
		if (!refugerepo.existsById(RefugeId)) {
			throw new ResourceNotFoundException("RefugeId " + RefugeId + " not found");
		}

		return photorepo.findById(PhotoId).map(photo -> {
			photorepo.delete(photo);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("PhotoId " + PhotoId + " not found"));
	}
}
