package com.bofugroup.service.adoption.business.dto;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "animal_tbl")
public class Animal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id_animal;
	@Column(name = "nickname", nullable = false)
	String nickname;
	@Column(name = "description", nullable = false)
	String description;
	@Column(name = "race", nullable = false)
	String race;
	@Column(name = "brith_date", nullable = true)
	Timestamp birth_date;
	@Column(name = "sterilization", nullable = false)
	boolean sterilization;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_animal")
	List<Photo_Animal> listPhotosAnimal;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_animal")
	List<Vaccine> listVaccine;

	@ManyToOne
	@JoinColumn(name = "id_refuge")
	Refuge refuge;

	public Animal() {
		super();
	}

	public Animal(String nickname, String description, String race, Timestamp birth_date,
			boolean sterilization,Refuge refuge) {
		super();
		this.nickname = nickname;
		this.description = description;
		this.race = race;
		this.birth_date = birth_date;
		this.sterilization = sterilization;
		this.refuge = refuge;
	}

	public Long getAnimalId() {
		return id_animal;
	}

	public void setAnimalId(Long animalId) {
		id_animal = animalId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public Timestamp getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(Timestamp birth_date) {
		this.birth_date = birth_date;
	}

	public boolean isSterilization() {
		return sterilization;
	}

	public void setSterilization(boolean sterilization) {
		this.sterilization = sterilization;
	}

	public Refuge getRefuge() {
		return refuge;
	}

	public void setRefuge(Refuge refuge) {
		this.refuge = refuge;
	}
}
