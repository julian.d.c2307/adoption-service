package com.bofugroup.service.adoption.facade.Imp;

import java.net.URI;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bofugroup.service.adoption.business.dto.Animal;
import com.bofugroup.service.adoption.business.srv.AnimalService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class AnimalController 
{
	
	@Autowired
	AnimalService animalservice;
	
	private static final Logger LOGGER =LoggerFactory.getLogger(AnimalController.class);
	
	/*@ApiOperation(value = "View a list of available refuges-Animal", response = ArrayList.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/api/refuges/{RefugeId}/Animals")
	public List<Animal> getAllAnimalByRefugeId(@PathVariable(value = "RefugeId") Long RefugeId,
			Pageable pageable) {
		LOGGER.info("Lista de animales");
		return animalservice.all(RefugeId,pageable);
	}*/

	@ApiOperation(value = "save a animal-refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully save in bd"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping("/api/refuges/{RefugeId}/Animal")
	public Animal createAnimal(@PathVariable(value = "RefugeId") Long RefugeId, @RequestBody Animal animal) {
		LOGGER.info("guardar animal a refugio en bd");
		return animalservice.saveAnimal(RefugeId, animal);
	}

	@ApiOperation(value = "update a animal-refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PutMapping("/api/refuges/{RefugeId}/Animal/{AnimalId}")
	public Animal updateAnimal(@PathVariable(value = "RefugeId") Long RefugeId,
			@PathVariable(value = "AnimalId") Long AnimalId, @Valid @RequestBody Animal animalRequest) {
		LOGGER.info("Actualizar en bd");
		return animalservice.update(RefugeId, AnimalId, animalRequest);
	}

	@ApiOperation(value = "Delete a Animal-refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@DeleteMapping("/api/refuges/{RefugeId}/Animal/{AnimalId}")
	public ResponseEntity<?> deleteAnimal(@PathVariable(value = "RefugeId") Long RefugeId,
			@PathVariable(value = "AnimlId") Long AnimalId) {
		LOGGER.info("eliminar de bd");
		return animalservice.delete(RefugeId, AnimalId);
	}
}
