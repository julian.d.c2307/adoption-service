package com.bofugroup.service.adoption.business.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "photo_animal")
public class Photo_Animal 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	@Column(name = "urlphoto", nullable = true)
	String urlphoto;
	
	@ManyToOne
	@JoinColumn(name = "id_animal")
	Animal animal;

	public Photo_Animal() {
		super();
	}

	public Photo_Animal( String urlphoto, Animal animal) {
		super();
		this.urlphoto = urlphoto;
		this.animal = animal;
	}

	public Long getPhotoId() {
		return id;
	}

	public void setPhotoId(Long photoId) {
		id = photoId;
	}

	public String getUrlphoto() {
		return urlphoto;
	}

	public void setUrlphoto(String urlphoto) {
		this.urlphoto = urlphoto;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}	
}
