package com.bofugroup.service.adoption.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "not found Animal")
public class ExceptionAnimalNotFound 
{
	private static final long serialVersionUID = 1L;
}
