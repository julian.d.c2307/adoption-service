package com.bofugroup.service.adoption.business.srv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.bofugroup.service.adoption.business.dto.Animal;
import com.bofugroup.service.adoption.exception.ResourceNotFoundException;
import com.bofugroup.service.adoption.repository.AnimalRepository;
import com.bofugroup.service.adoption.repository.RefugeRepository;

@Service
public class AnimalService {
	
	@Autowired
	AnimalRepository animalrepo;
	
	@Autowired
	RefugeRepository refugerepo;
	
	
	/*public List<Animal>all(Long RefugeId,Pageable pageable)
	{
		return (List<Animal>) animalrepo.findRefugetId(RefugeId, pageable);
	}*/
	
	public Animal saveAnimal(Long RefugeId,@RequestBody Animal animal) {
	return refugerepo.findById(RefugeId).map(refuge -> {
		animal.setRefuge(refuge);
		return animalrepo.save(animal);
	}).orElseThrow(() -> new ResourceNotFoundException("Refuge " + RefugeId + " not found"));
	}
	
	public Animal update(Long RefugeId,Long AnimalId,@RequestBody Animal animalRequest) {
	if (!refugerepo.existsById(RefugeId)) {
		throw new ResourceNotFoundException("RefugeId " + RefugeId + " not found");
	}

	return animalrepo.findById(AnimalId).map(animal -> {
		animal.setBirth_date(animalRequest.getBirth_date());
		animal.setDescription(animalRequest.getDescription());
		animal.setNickname(animalRequest.getNickname());
		animal.setRace(animalRequest.getRace());
		animal.setSterilization(animalRequest.isSterilization());
		
		return animalrepo.save(animal);
	}).orElseThrow(() -> new ResourceNotFoundException("AnimalId " + AnimalId + "not found"));
	}
	
	public ResponseEntity<?> delete(Long RefugeId,Long AnimalId){ 
	if (!refugerepo.existsById(RefugeId)) {
		throw new ResourceNotFoundException("RefugeId " + RefugeId + " not found");
	}

	return animalrepo.findById(AnimalId).map(animal -> {
		animalrepo.delete(animal);
		return ResponseEntity.ok().build();
	}).orElseThrow(() -> new ResourceNotFoundException("AnimalId " + AnimalId + " not found"));

	}
	}
	
