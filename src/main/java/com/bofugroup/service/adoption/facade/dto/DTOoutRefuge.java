package com.bofugroup.service.adoption.facade.dto;

import java.sql.Timestamp;

public class DTOoutRefuge 
{
	Long id;
	String name;
	String direction;
	Timestamp fundation_date;
	String description;
	Long id_Ciudad;
	Long id_User;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public Timestamp getFundation_date() {
		return fundation_date;
	}
	public void setFundation_date(Timestamp fundation_date) {
		this.fundation_date = fundation_date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId_Ciudad() {
		return id_Ciudad;
	}
	public void setId_Ciudad(Long id_Ciudad) {
		this.id_Ciudad = id_Ciudad;
	}
	public Long getId_User() {
		return id_User;
	}
	public void setId_User(Long id_User) {
		this.id_User = id_User;
	}

}
