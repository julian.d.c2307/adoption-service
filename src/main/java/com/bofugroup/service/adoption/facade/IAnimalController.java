package com.bofugroup.service.adoption.facade;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bofugroup.service.adoption.facade.dto.DTOinAnimal;

public interface IAnimalController 
{
	
	public ResponseEntity<DTOinAnimal>agregarAnimalEnRefugio(Long idRefuge,DTOinAnimal animal);
	public HttpStatus eliminarAnimalDeRefugio(Long idRefuge,Long idAnimal);
	public HttpStatus actualizarAnimalEnRefugio(Long idRefuge, DTOinAnimal dtoinAnimal);
	
}
