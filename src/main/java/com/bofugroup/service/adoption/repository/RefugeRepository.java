package com.bofugroup.service.adoption.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bofugroup.service.adoption.business.dto.Refuge;

@Repository
public interface RefugeRepository extends CrudRepository<Refuge, Long>
{

}
