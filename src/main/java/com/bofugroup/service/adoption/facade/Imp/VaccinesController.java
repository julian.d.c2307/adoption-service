package com.bofugroup.service.adoption.facade.Imp;

import java.net.URI;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bofugroup.service.adoption.business.dto.Vaccine;
import com.bofugroup.service.adoption.business.srv.VaccineService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class VaccinesController {

	@Autowired
	VaccineService vaccineservice;
	
	private static final Logger LOGGER =LoggerFactory.getLogger(VaccinesController.class);
	
	/*@ApiOperation(value = "View a list of available Animals-Vaccines", response = ArrayList.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/api/refuges/{RefugeId}/Animals/Vaccines")
	public List<Vaccine> getAllVacinnesByAnimalId(@PathVariable(value = "RefugeId") Long RefugeId, @PathVariable(value = "AnimalId") Long AnimalId,
			Pageable pageable) {
		LOGGER.info("lista de vacunas");
		return vaccineservice.all(RefugeId, AnimalId, pageable);
	}*/

	@ApiOperation(value = "save a Vaccine-Animal in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully save in bd"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping("/api/refuges/{RefugeId}/Animal/{AnimalId}/Vaccines")
	public Vaccine createVaccineAnimal(@PathVariable(value = "RefugeId") Long RefugeId,@PathVariable(value = "AnimalId") Long AnimalId, 
			@RequestBody Vaccine vaccine) {
		LOGGER.info("Guardar vacuna a animal en refugio en bd");
		return vaccineservice.saveVaccine(RefugeId, AnimalId, vaccine);
	}

	@ApiOperation(value = "update a Vaccine-Animal in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PutMapping("/api/refuges/{RefugeId}/Animals/{AnimalId}/Vaccine/{VaccineId}")
	public Vaccine updateVaccineAnimals(@PathVariable(value = "RefugeId") Long RefugeId,
			@PathVariable(value = "AnimalId") Long AnimalId,
			@PathVariable(value = "VaccineId") Long VaccineId, @Valid @RequestBody Vaccine vaccineRequest) {
		LOGGER.info("actualizar vacunas");
		return vaccineservice.update(RefugeId, AnimalId, VaccineId, vaccineRequest);
	}

	@ApiOperation(value = "Delete a Vaccine-Animal in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@DeleteMapping("/api/refuges/{RefugeId}/Animals/{AnimalId}/Vaccine/{VaccineId}")
	public ResponseEntity<?> deletevaccine(@PathVariable(value = "RefugeId") Long RefugeId,
			@PathVariable(value = "AnimalId") Long AnimalId,
			@PathVariable(value = "VaccineId") Long VaccineId) {
		LOGGER.info("eliminar vacuna");
		return vaccineservice.delete(RefugeId, AnimalId, VaccineId);
	}
	
	
	
}
