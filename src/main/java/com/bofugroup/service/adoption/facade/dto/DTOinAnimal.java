package com.bofugroup.service.adoption.facade.dto;

import java.sql.Timestamp;

public class DTOinAnimal 
{
	Long id;
	String nickname;
	String description;
	String race;
	Timestamp birth_date;
	boolean sterilization;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public Timestamp getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(Timestamp birth_date) {
		this.birth_date = birth_date;
	}
	public boolean isSterilization() {
		return sterilization;
	}
	public void setSterilization(boolean sterilization) {
		this.sterilization = sterilization;
	}
	
	
}
