package com.bofugroup.service.adoption.business.srv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import com.bofugroup.service.adoption.business.dto.Photo_Animal;
import com.bofugroup.service.adoption.exception.ResourceNotFoundException;
import com.bofugroup.service.adoption.repository.AnimalRepository;
import com.bofugroup.service.adoption.repository.Photo_AnimalRepository;
import com.bofugroup.service.adoption.repository.RefugeRepository;


public class Photo_AnimalService {

	@Autowired
	Photo_AnimalRepository photoArepo;
	
	@Autowired
	AnimalRepository animalrepo;
	
	@Autowired
	RefugeRepository refugerepo;
	
	public Photo_Animal savePhotoAnimal(Long RefugeId,Long AnimalId,Photo_Animal photoanimal) {
		if (!refugerepo.existsById(RefugeId)) {
			throw new ResourceNotFoundException("RefugeId " + RefugeId + " not found");
		} 
		return animalrepo.findById(AnimalId).map(animal -> {
			photoanimal.setAnimal(animal);
			return photoArepo.save(photoanimal);
		}).orElseThrow(() -> new ResourceNotFoundException("animalId " + AnimalId + " not found"));
	}
	
	public Photo_Animal updatePhotoAnimal(Long RefugeId,Long AnimalId,Long PhotoId,@RequestBody Photo_Animal photoanimalRequest) {
		if (!refugerepo.existsById(RefugeId)) {
			throw new ResourceNotFoundException("Refuge" + RefugeId + " not found");
		}else if(!!animalrepo.existsById(AnimalId)) {
			throw new ResourceNotFoundException("AnimalId" + AnimalId + "not found");
		}

		return photoArepo.findById(PhotoId).map(photoanimal -> {
			photoanimal.setUrlphoto(photoanimalRequest.getUrlphoto());
			return photoArepo.save(photoanimal);
		}).orElseThrow(() -> new ResourceNotFoundException("PhotoId " + PhotoId + "not found"));
	}
	
	public ResponseEntity<?> deletePhotoAnimal(Long RefugeId,Long AnimalId,Long PhotoId){
		if (!refugerepo.existsById(RefugeId)) {
			throw new ResourceNotFoundException("Refuge" + RefugeId + " not found");
		}else if(!!animalrepo.existsById(AnimalId)) {
			throw new ResourceNotFoundException("AnimalId" + AnimalId + "not found");
		}
		return photoArepo.findById(PhotoId).map( photoanimal-> {
			photoArepo.delete(photoanimal);
			return ResponseEntity.ok().build();
		}).orElseThrow(()-> new ResourceNotFoundException("PhotoId"+ PhotoId + "not found"));
	}
}
