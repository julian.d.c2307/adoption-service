package com.bofugroup.service.adoption.facade.mapper.Imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bofugroup.service.adoption.business.dto.Animal;
import com.bofugroup.service.adoption.facade.dto.DTOinAnimal;
import com.bofugroup.service.adoption.facade.dto.DTOoutAnimal;
import com.bofugroup.service.adoption.facade.mapper.IAnimalMapper;

@Component("AnimalMapper")
public class AnimalMapper implements IAnimalMapper
{

	@Override
	public DTOoutAnimal mapOutAnimal(Animal animal) {
		DTOoutAnimal response = new DTOoutAnimal();
		if (animal != null)
		{
			response.setId(animal.getAnimalId());
			response.setNickname(animal.getNickname());
			response.setRace(animal.getRace());
			response.setBirth_date(animal.getBirth_date());
			response.setSterilization(animal.isSterilization());
			response.setDescription(animal.getDescription());
		}
		return response;
	}

	@Override
	public Animal mapInAnimal(DTOinAnimal dtoinAnimal) {
		
		Animal animal = new Animal();
		if(dtoinAnimal != null)
		{
			animal.setAnimalId(dtoinAnimal.getId());
			animal.setNickname(dtoinAnimal.getNickname());
			animal.setRace(dtoinAnimal.getRace());
			animal.setBirth_date(dtoinAnimal.getBirth_date());
			animal.setSterilization(dtoinAnimal.isSterilization());
			animal.setDescription(dtoinAnimal.getDescription());
		}
		return animal;
	}

	@Override
	public List<DTOoutAnimal> mapOutListAnimal(List<Animal> animal) {
		List<DTOoutAnimal> response = new ArrayList<DTOoutAnimal>();
		
		if(animal != null && !animal.isEmpty())
		{
			for (Animal temp : animal) 
			{
				response.add(mapOutAnimal(temp));
			}
		}
		return response;
	}

}
