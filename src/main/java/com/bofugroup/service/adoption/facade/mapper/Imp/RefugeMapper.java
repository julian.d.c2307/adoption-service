package com.bofugroup.service.adoption.facade.mapper.Imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bofugroup.service.adoption.business.dto.Refuge;
import com.bofugroup.service.adoption.facade.dto.DTOinRefuge;
import com.bofugroup.service.adoption.facade.dto.DTOoutRefuge;
import com.bofugroup.service.adoption.facade.mapper.IRefugeMapper;

@Component("RefugeMapper")
public class RefugeMapper implements IRefugeMapper
{

	@Override
	public DTOoutRefuge mapOutRefuge(Refuge refuge) 
	{
		DTOoutRefuge response = new DTOoutRefuge();
		if(refuge != null)
		{
			response.setId(refuge.getRefugeId());
			response.setName(refuge.getName());
			response.setDescription(refuge.getDescription());
			response.setDirection(refuge.getDirection());
			response.setFundation_date(refuge.getFundation_date());
			response.setId_Ciudad(refuge.getId_Ciudad());
			response.setId_User(refuge.getId_User());
		}
		return response;
	}

	@Override
	public Refuge mapInRefuge(DTOinRefuge dtoInRefuge) 
	{
		Refuge refuge = new Refuge();
		if(dtoInRefuge != null)
		{
			refuge.setName(dtoInRefuge.getName());
			refuge.setDescription(dtoInRefuge.getDescription());
			refuge.setDirection(dtoInRefuge.getDirection());
			refuge.setFundation_date(dtoInRefuge.getFundation_date());
			refuge.setId_Ciudad(dtoInRefuge.getId_Ciudad());
			refuge.setId_User(dtoInRefuge.getId_User());
		}
		return refuge;
	}

	@Override
	public List<DTOoutRefuge> mapOutListRefuge(List<Refuge> refuge) 
	{
		List<DTOoutRefuge> response = new ArrayList<DTOoutRefuge>();
		
		if(refuge != null && !refuge.isEmpty())
		{
			for(Refuge temp : refuge)
			{
				response.add(mapOutRefuge(temp));
			}
		}
		
		return response;
	}
	
}
