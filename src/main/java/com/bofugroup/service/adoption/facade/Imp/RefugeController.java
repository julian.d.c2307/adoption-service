package com.bofugroup.service.adoption.facade.Imp;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bofugroup.service.adoption.business.srv.RefugeService;
import com.bofugroup.service.adoption.facade.IRefugeController;
import com.bofugroup.service.adoption.facade.dto.DTOinRefuge;
import com.bofugroup.service.adoption.facade.dto.DTOoutRefuge;
import com.bofugroup.service.adoption.facade.mapper.IRefugeMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/api/refuges")
@RestController
@Api(value = "Refuge Resource", produces = "application/json")
public class RefugeController implements IRefugeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(RefugeController.class);

	@Autowired
	IRefugeMapper mapper;

	@Autowired
	RefugeService refugeService;

	@ApiOperation(value = "View a list of available refuges", response = ArrayList.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<List<DTOoutRefuge>> all() {
		List<DTOoutRefuge> refuges = new ArrayList<>();
		LOGGER.info("init return list of refuges");
		refuges = mapper.mapOutListRefuge(refugeService.all());
		return new ResponseEntity<List<DTOoutRefuge>>(refuges, HttpStatus.OK);
	}

	/*
	 * @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces =
	 * "application/json") public ResponseEntity<DTOoutRefuge>
	 * findByRefuge(@PathVariable("refugeId")Long refugeId) {
	 * LOGGER.error("has errors Refuge"); return new
	 * ResponseEntity<DTOoutRefuge>(new DTOoutRefuge(),HttpStatus.OK); }
	 */

	@ApiOperation(value = "save a refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully save in bd"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@Override
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> agregarRefugio(@RequestBody DTOinRefuge dtoInRefuge) {
		DTOoutRefuge response = null;
		response = mapper.mapOutRefuge(refugeService.save(mapper.mapInRefuge(dtoInRefuge)));
		if (response != null) {
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(response.getId()).toUri();

			return ResponseEntity.created(location).build();
		} else {
			LOGGER.error("has errors establishment");
			return ResponseEntity.noContent().build();
		}

	}

	@ApiOperation(value = "update a refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@Override
	@RequestMapping(value = "/update/{idRefuge}", method = { RequestMethod.GET,
			RequestMethod.PUT }, produces = "application/json")
	public HttpStatus updateRefuge(@PathVariable("idRefuge") Long idRefuge, @RequestBody DTOinRefuge dtoInRefuge) {
		DTOoutRefuge response = null;

		response = mapper.mapOutRefuge(refugeService.update(idRefuge, mapper.mapInRefuge(dtoInRefuge)));
		LOGGER.info("actualizando base de datos");
		return HttpStatus.OK;
	}

	@ApiOperation(value = "Delete a refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@Override
	@RequestMapping(value = "/delete/{idRefuge}", method = RequestMethod.DELETE, produces = "application/json")
	public HttpStatus deleteRefuge(@PathVariable("idRefuge") Long idRefuge) {
		LOGGER.info("eliminando de la base de datos");
		refugeService.delete(idRefuge);
		return HttpStatus.OK;
	}

}
