package com.bofugroup.service.adoption.repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bofugroup.service.adoption.business.dto.Photo_Refuge;

@Repository
public interface Photo_RefugeRepository extends CrudRepository<Photo_Refuge, Long>
{
	//List<Photo_Refuge> findByRefugeId(Long id,Pageable pageable);
}
