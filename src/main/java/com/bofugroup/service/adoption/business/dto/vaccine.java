package com.bofugroup.service.adoption.business.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vaccine_animal")
public class Vaccine 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	@Column(name = "name_vaccine", nullable = true)
	String name;
	
	@ManyToOne
	@JoinColumn(name = "id_animal")
	Animal animal;

	public Vaccine() {
		super();
	}

	public Vaccine( String name, Animal animal) {
		super();
		this.name = name;
		this.animal = animal;
	}

	public Long getVaccineId() {
		return id;
	}

	public void setVaccineId(Long vaccineId) {
		id = vaccineId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	
	

	
}
