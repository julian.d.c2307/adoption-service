package com.bofugroup.service.adoption.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bofugroup.service.adoption.business.dto.Animal;


@Repository
public interface AnimalRepository extends CrudRepository<Animal,Long>
{
	//List<Animal> findRefugetId(Long refugeId, Pageable pageable);
}
