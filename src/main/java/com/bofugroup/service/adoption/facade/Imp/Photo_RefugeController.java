package com.bofugroup.service.adoption.facade.Imp;

import java.net.URI;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.bofugroup.service.adoption.business.dto.Photo_Refuge;
import com.bofugroup.service.adoption.business.srv.Photo_RefugeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class Photo_RefugeController {

	@Autowired
	Photo_RefugeService photoservice;
	
	private static final Logger LOGGER =LoggerFactory.getLogger(Photo_RefugeController.class);
	
	/*@ApiOperation(value = "View a list of available refuges-Photos", response = ArrayList.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/api/refuges/{RefugeId}/Photos")
	public List<Photo_Refuge> getAllPhotosByRefugeId(@PathVariable(value = "RefugeId") Long RefugeId,
			Pageable pageable) {
		return photoservice.all(RefugeId, pageable);
	}*/

	@ApiOperation(value = "save a photo-refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully save in bd"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping("/api/refuges/{RefugeId}/Photos")
	public Photo_Refuge createphoto(@PathVariable(value = "RefugeId") Long RefugeId, @RequestBody Photo_Refuge photo) {
		LOGGER.info("guardar foto a refugio en bd");
		return photoservice.savePhoto(RefugeId, photo);
	}

	@ApiOperation(value = "update a photo-refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PutMapping("/api/refuges/{RefugeId}/Photos/{PhotoId}")
	public Photo_Refuge updatePhoto(@PathVariable(value = "RefugeId") Long RefugeId,
			@PathVariable(value = "PhotoId") Long PhotoId, @Valid @RequestBody Photo_Refuge photoRequest) {
		LOGGER.info("actualizar in bd");
		return photoservice.update(RefugeId, PhotoId, photoRequest);
	}

	@ApiOperation(value = "Delete a photo-refuge in db", response = URI.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@DeleteMapping("/api/refuges/{RefugeId}/Photos/{PhotoId}")
	public HttpStatus deletePhoto(@PathVariable(value = "RefugeId") Long RefugeId,
			@PathVariable(value = "PhotoId") Long PhotoId) {
		LOGGER.info("eliminando de la base de datos");
		photoservice.delete(RefugeId, PhotoId);
		return  HttpStatus.OK;
	}

}
