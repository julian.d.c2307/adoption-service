package com.bofugroup.service.adoption.facade.mapper;

import java.util.List;

import com.bofugroup.service.adoption.business.dto.Animal;
import com.bofugroup.service.adoption.facade.dto.DTOinAnimal;
import com.bofugroup.service.adoption.facade.dto.DTOoutAnimal;



public interface IAnimalMapper 
{
	DTOoutAnimal mapOutAnimal(Animal animal);
	Animal mapInAnimal(DTOinAnimal dtOinAnimal);
	List<DTOoutAnimal> mapOutListAnimal(List<Animal> animal);
	
}
