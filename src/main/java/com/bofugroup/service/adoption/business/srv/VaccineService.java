package com.bofugroup.service.adoption.business.srv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.bofugroup.service.adoption.business.dto.Vaccine;
import com.bofugroup.service.adoption.exception.ResourceNotFoundException;
import com.bofugroup.service.adoption.repository.AnimalRepository;
import com.bofugroup.service.adoption.repository.RefugeRepository;
import com.bofugroup.service.adoption.repository.VaccineRepository;

@Service
public class VaccineService {

	@Autowired
	VaccineRepository vaccinerepo;

	@Autowired
	AnimalRepository animalrepo;

	@Autowired
	RefugeRepository refugerepo;

	/*public List<Vaccine> all(Long RefugeId, Long AnimalId, Pageable pageable) {
		return (List<Vaccine>) vaccinerepo.findbyRAId(RefugeId, AnimalId, pageable);
	}*/

	public Vaccine saveVaccine(Long RefugeId, Long AnimalId, Vaccine vaccine) {
		if (!refugerepo.existsById(RefugeId)) {
			throw new ResourceNotFoundException("RefugeId " + RefugeId + " not found");
		} 
		return animalrepo.findById(AnimalId).map(animal -> {
			vaccine.setAnimal(animal);
			return vaccinerepo.save(vaccine);
		}).orElseThrow(() -> new ResourceNotFoundException("animalId " + AnimalId + " not found"));
	}

	public Vaccine update(Long RefugeId, Long AnimalId, Long VaccineId, @RequestBody Vaccine vaccineRequest) {
		if (!refugerepo.existsById(RefugeId)) {
			throw new ResourceNotFoundException("Refuge" + RefugeId + " not found");
		}else if(!!animalrepo.existsById(AnimalId)) {
			throw new ResourceNotFoundException("AnimalId" + AnimalId + "not found");
		}

		return vaccinerepo.findById(VaccineId).map(vaccine -> {
			vaccine.setName(vaccineRequest.getName());
			return vaccinerepo.save(vaccine);
		}).orElseThrow(() -> new ResourceNotFoundException("VaccineId " + VaccineId + "not found"));
	}

	public ResponseEntity<?> delete(Long RefugeId, Long AnimalId, Long VaccineId) {
		if (!refugerepo.existsById(RefugeId)) {
			throw new ResourceNotFoundException("Refuge" + RefugeId + " not found");
		}else if(!!animalrepo.existsById(AnimalId)) {
			throw new ResourceNotFoundException("AnimalId" + AnimalId + "not found");
		}

		return vaccinerepo.findById(VaccineId).map(vaccine -> {
			vaccinerepo.delete(vaccine);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("VaccineId " + VaccineId + " not found"));
	}
}
